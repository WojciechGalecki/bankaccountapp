package bank;

public class BankRequest implements Runnable {

    private double tmpMoney;

    public BankRequest(double tmpMoney) {
        this.tmpMoney = tmpMoney;
    }

    @Override
    public void run() {
        try{
            if(tmpMoney < 0.0) {
                Thread.sleep(1);
                BankAccount.getInstance().sub(-tmpMoney);
            } else if (tmpMoney > 0.0){
                Thread.sleep(1);
                BankAccount.getInstance().add(tmpMoney);
            } else if(tmpMoney == 0.0) {
                Thread.sleep(1);
                BankAccount.getInstance().balance();
            } else{
                System.out.println("Wrong operation!");
            }
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
