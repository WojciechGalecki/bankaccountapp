package bank;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Bank {

    private Executor bankOperations = Executors.newFixedThreadPool(1);
    private BankRequest newRequest;


    public void sendSubstractRequest(double howMuch){
        newRequest = new BankRequest(-howMuch);
        bankOperations.execute(newRequest);
    }

    public void sendAddToAccountRequest(double howMuch){
        newRequest = new BankRequest(howMuch);
        bankOperations.execute(newRequest);
    }

    public void printBalanceRequest(){
        newRequest = new BankRequest(0.0);
        bankOperations.execute(newRequest);
    }
}
