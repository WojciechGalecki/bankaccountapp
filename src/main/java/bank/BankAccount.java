package bank;

public class BankAccount {

    private double account = 0.0;

    private static BankAccount INSTANCE;

    public static synchronized BankAccount getInstance(){
        if(INSTANCE == null){
            synchronized (BankAccount.class){
                if(INSTANCE == null){
                    INSTANCE = new BankAccount();
                }
            }
        }
        return INSTANCE;
    }

    private BankAccount() {
    }


    public void add(double money){
        account += money;
        System.out.println("Added " + money + " PLN");
    }

    public void sub(double money) {
        if (money > account) {
            System.out.println("Insufficient funds on the account!");
            balance();
        } else {
            account -= money;
            System.out.println("Subtracted " + money + " PLN");
        }
    }

    public void balance(){
        System.out.println("Account balance: " + account + " PLN");
    }

}
